//
//  BaseUIViewController.swift
//  Core
//
//  Created by Aditi Jain 3 on 28/05/22.
//

import Foundation

public struct PlistReader {
    
    static var host: String {
        var host = ""
        do {
            host = try Configuration.value(for: "API_Host") as String
        } catch(let error) {
            debugPrint("Error :\(error)")
        }
        return host
    }
    
    
}

public enum Configuration {
    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    static func value<T>(for key: String) throws -> T where T: LosslessStringConvertible {
        guard let object = Bundle.main.object(forInfoDictionaryKey:key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            throw Error.invalidValue
        }
    }
}
